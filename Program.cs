﻿using OOPLab4.classes.helpers;
using OOPLab4.classes.road;
using OOPLab4.classes.road_objects.crosswalk;
using OOPLab4.classes.road_objects.traffic_lights;
using OOPLab4.classes.road_objects.traffic_lights.warning_traffic_lights;

Road road = new(
	4,
	60,
	1_000_000,
	new List<TrafficLights>(new TrafficLights[] {
		new(20), new(10), new(15), new WarningTrafficLights()
	}),
	new List<CrossWalk>()
);
var cars = RandomCar.GenerateMany(5, 10);

Simulate.CarFlow(road, cars);
Simulate.MaxCapacity(road);
