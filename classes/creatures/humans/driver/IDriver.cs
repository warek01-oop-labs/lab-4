using OOPLab4.classes.vehicles.Cars;

namespace OOPLab4.classes.creatures.humans.driver; 

public interface IDriver {
	public void SitInCar(Car car);
	public void Say();
}
