namespace OOPLab4.creatures.human; 

public interface IHuman {
	public void Walk();
	public void Stop();
	public void Present();
	public void Eat();
	public void Run();
	public void Grow();
	public void ChangeJob(string job);
	public void Say();
}
