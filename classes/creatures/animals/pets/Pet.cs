using OOPLab4.creatures.human;

namespace OOPLab4.creatures.animal.pets;

public class Pet : Animal, IPet {
	public Human BelongsTo;

	public Pet(Human     belongsTo, string species, string sound, int legsCount, AnimalType type,
	           DateTime? birthDate) : base(
		species, sound, legsCount, type, birthDate) {
		BelongsTo = belongsTo;
	}
}
