using OOPLab4.classes.vehicles.Cars;

namespace OOPLab4.classes.road;

public interface IRoad {
	public void FlowIn(List<Car> cars);
	public void FlowIn(Car       car);
	public void FlowOut(int      count);
	public void CarLeave(Car     car);
	public void Clear();
}
