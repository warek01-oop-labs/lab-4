using OOPLab4.classes.root;
using OOPLab4.classes.road_objects.crosswalk;
using OOPLab4.classes.road_objects.traffic_lights;
using OOPLab4.classes.vehicles.Cars;

namespace OOPLab4.classes.road;

public class Road : Unmovable, IRoad {
	private List<Car>           _carsList          = new();
	private List<CrossWalk>     _crossWalksList    = new();
	private List<TrafficLights> _trafficLightsList = new();

	public int BandsCount { get; }

	public int SpeedLimit { get; }

	public int Length { get; }

	public bool IsFull {
		get {
			double total = 0;
			foreach (var car in _carsList) {
				total += car.Length / BandsCount;
			}

			return total > Length;
		}
	}

	public List<Car> Cars => _carsList;

	public List<TrafficLights> TrafficLights => _trafficLightsList;

	public List<CrossWalk> CrossWalk => _crossWalksList;

	public int CarsCount => _carsList.Count;

	public Road(int             bandsCount, int speedLimit, int length, List<TrafficLights> trafficLights,
	            List<CrossWalk> crossWalks) {
		BandsCount         = bandsCount;
		SpeedLimit         = speedLimit;
		Length             = length;
		_trafficLightsList = trafficLights;
		_crossWalksList    = crossWalks;
	}

	public void FlowIn(List<Car> cars) {
		_carsList.AddRange(cars);
	}

	public void FlowIn(Car car) {
		_carsList.Add(car);
	}

	public void FlowOut(int count) {
		if (count < 0)
			throw new Exception("Count can't be negative");

		if (count > CarsCount) {
			_carsList.Clear();
			return;
		}

		for (int i = 0; i < count; i++)
			_carsList.RemoveAt(0);
	}

	public void CarLeave(Car car) {
		_carsList.RemoveAt(
			_carsList.FindIndex(c => c == car)
		);
	}

	public void Clear() {
		_carsList.Clear();
	}
}
