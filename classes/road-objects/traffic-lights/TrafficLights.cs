using OOPLab4.classes.road;

namespace OOPLab4.classes.road_objects.traffic_lights;

public class TrafficLights : RoadObject, ITrafficLights {
	public int RedLightTiming { get; }

	public TrafficLights(int redLightTiming) {
		RedLightTiming = redLightTiming;
	}
}
