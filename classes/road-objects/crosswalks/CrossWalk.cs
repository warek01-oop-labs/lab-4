using OOPLab4.creatures;

namespace OOPLab4.classes.road_objects.crosswalk;

public class CrossWalk : RoadObject, ICrossWalk {
	private List<Creature> _peds = new();

	public int PedsCount => _peds.Count;
	
	public CrossWalk() { }

	public void Cross() {
		_peds.Clear();
	}
}
