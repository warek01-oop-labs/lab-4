namespace OOPLab4.classes.vehicles.Cars;

public abstract class Car : Vehicle, ICar {
	public static readonly int SpaceBetweenFrontCar = 100;

	private readonly string _carType;
	private readonly int    _maxSpeed  = 200;
	private          bool   _isLocked  = true;
	private          bool   _isDriving = false;

	public int MaxSpeed {
		get => _maxSpeed;
		init {
			if (value < 0) throw new Exception("Speed can't me negative");
			_maxSpeed = value;
		}
	}

	public int Height    { get; init; }
	public int Width     { get; init; }
	public int Clearance { get; init; }
	public int Length    { get; init; }

	public string BrandName { get; }

	public string CarInfo =>
		$"{BrandName} is {_carType}, has {DoorsCount} doors, {Weight} kg, max speed of {MaxSpeed} km/h";

	public string Position => $"X:{X}, Y:{Y}";

	public Car(int doorsCount, string type, string brandName)
		: base(doorsCount, 4, "Car", 1200) {
		_carType   = type;
		BrandName = brandName;
	}

	public void Drive() {
		Console.WriteLine(BrandName + (
			!_isLocked
				? " is driving"
				: " can't drive, it's locked"
		));

		if (!_isLocked) {
			_isDriving = true;
		}
	}

	public void DriveTo(double x, double y) {
		if (_isLocked) {
			Console.WriteLine($"{BrandName} is locked");
			return;
		}

		Console.WriteLine($"{BrandName} is driving to {x}, {y} ...");
		Console.WriteLine($"{BrandName} has arrived to to {x}, {y}");
		ChangePosition(x, y);
		_isDriving = false;
	}

	public void StopDriving() {
		Console.WriteLine(BrandName + (
				_isDriving
					? " has stopped"
					: " is already stopped"
			)
		);

		_isDriving = false;
	}

	public void Lock() {
		_isLocked = true;
	}

	public void Unlock() {
		_isLocked = false;
	}

	public virtual void PresentCar() {
		Console.WriteLine(CarInfo);
	}
}
