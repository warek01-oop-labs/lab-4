namespace OOPLab4.classes.vehicles.Cars; 

public interface ICar {
	public void Lock();
	public void Unlock();
	public void Drive();
	public void DriveTo(double x, double y);
	public void StopDriving();
	public void PresentCar();
}
