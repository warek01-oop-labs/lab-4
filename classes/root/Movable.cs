namespace OOPLab4.classes.root; 

public class Movable: Entity, IMovable {
	public double X => _x;
	public double Y => _y;

	protected void ChangePosition(double x, double y) {
		_x = x;
		_y = y;
	}

}
