using OOPLab4.classes.vehicles.Cars;

namespace OOPLab4.classes.helpers;

public class RandomCar {
	private static readonly string[] Types = {
		"Hatchback",
		"Sedan",
		"Crossover",
		"Suv",
		"Universal",
		"Coupe",
	};

	private static readonly string[] BrandNames = {
		"Toyota", "Mercedes", "Nissan", "Volkswagen", "Audi", "Skoda", "Renault", "Dacia", "BMW", "Mazda",
		"Citroen", "Lada", "Seat", "Chevrolet", "Saab", "Chrysler", "Kia", "Hyundai", "Ford", "Lancia"
	};

	private static string GenerateBrandName() {
		Random rand = new();

		return BrandNames[rand.Next(0, BrandNames.Length)];
	}

	public static Car Generate() {
		Random rand = new();
		int    r    = rand.Next(0, Types.Length);
		return Types[r] switch {
			"Hatchback" => new Hatchback(
				GenerateBrandName(),
				Convert.ToBoolean(rand.Next(0, 2))
			) {
				Weight    = rand.Next(800, 1500),
				Height    = rand.Next(150, 170),
				Clearance = rand.Next(10,  20),
				Width     = rand.Next(150, 160),
				MaxSpeed  = rand.Next(160, 240),
				Length = rand.Next(3_500, 4_000),
			},
			"Sedan" => new Sedan(GenerateBrandName()) {
				Weight    = rand.Next(900,   2000),
				Height    = rand.Next(150,   170),
				Clearance = rand.Next(10,    20),
				Width     = rand.Next(150,   160),
				MaxSpeed  = rand.Next(160,   260),
				Length    = rand.Next(3_700, 4_200),
			},
			"Crossover" => new Crossover(GenerateBrandName()) {
				Weight    = rand.Next(900,   2000),
				Height    = rand.Next(150,   170),
				Clearance = rand.Next(10,    20),
				Width     = rand.Next(150,   160),
				MaxSpeed  = rand.Next(160,   220),
				Length    = rand.Next(3_700, 4_200),
			},
			"Suv" => new Suv(GenerateBrandName()) {
				Weight    = rand.Next(1200,  3000),
				Height    = rand.Next(160,   200),
				Clearance = rand.Next(20,    40),
				Width     = rand.Next(160,   180),
				MaxSpeed  = rand.Next(140,   220),
				Length    = rand.Next(3_800, 4_500),
			},
			"Universal" => new Universal(GenerateBrandName()) {
				Weight    = rand.Next(900,   2000),
				Height    = rand.Next(150,   170),
				Clearance = rand.Next(10,    20),
				Width     = rand.Next(150,   170),
				MaxSpeed  = rand.Next(160,   240),
				Length    = rand.Next(3_700, 4_300),
			},
			"Coupe" => new Coupe(GenerateBrandName()) {
				Weight    = rand.Next(800,   1400),
				Height    = rand.Next(140,   170),
				Clearance = rand.Next(10,    20),
				Width     = rand.Next(140,   150),
				MaxSpeed  = rand.Next(160,   280),
				Length    = rand.Next(3_000, 4_000),
			},
		};
	}

	public static List<Car> GenerateMany(int min, int max) {
		Random    rand   = new();
		int       amount = rand.Next(min, max);
		List<Car> list   = new();

		for (int i = 0; i < amount; i++) 
			list.Add(Generate());

		return list;
	}
}
