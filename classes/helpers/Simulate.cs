using OOPLab4.classes.road;
using OOPLab4.classes.vehicles.Cars;

namespace OOPLab4.classes.helpers;

public class Simulate {
	public static void CarFlow(Road road, List<Car> cars) {
		Random rand = new();
		road.FlowIn(cars);

		foreach (var car in cars) {
			int    speed = Math.Min(road.SpeedLimit, car.MaxSpeed);
			double time  = Convert.ToDouble(speed * 1_000_000) / road.Length;

			foreach (var light in road.TrafficLights)
				if (Convert.ToBoolean(rand.Next(0, 2))) {
					Console.WriteLine($"{car.BrandName} passed on green light");
				}
				else {
					time += light.RedLightTiming;
					Console.WriteLine($"{car.BrandName} stopped on red light");
				}

			Console.WriteLine($"{car.CarInfo}");
			Console.WriteLine($"leaves road in {time} s\n");

			road.CarLeave(car);
		}
	}

	public static void MaxCapacity(Road road) {
		int availSpace = road.Length * road.BandsCount;
		int takenSpace = 0;

		road.Clear();

		while (takenSpace < availSpace) {
			Car car = RandomCar.Generate();
			takenSpace += car.Length + Car.SpaceBetweenFrontCar;
			road.FlowIn(car);
		}

		Console.WriteLine($"Road can fit ~ {road.CarsCount} cars");
	}
}
